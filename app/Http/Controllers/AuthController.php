<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('page.register');
    }
    public function signup(Request $request){
        $FirstName = $request->FirstName;
        $LastName = $request->LastName;
        return view('page.welcome', compact('FirstName', 'LastName'));
    }
}
