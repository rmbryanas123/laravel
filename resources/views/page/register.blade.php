<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign up Form</h3><br>
    <form action='/post' method='POST'>
        @csrf
        <label> First Name : </label><br><br>
        <input type="text" name="FirstName"><br><br>
        <label> Last Name : </label><br><br>
        <input type="text" name="LastName"><br><br>
        <label for="gender">Gender :</label> <br>
        <input type="radio" value="Male" name="gender"> Male <br>
        <input type="radio" value="Female" name="gender"> Female <br><br>
        <label for="nat">Nationality :</label> <br><br>
        <select name="nat" id="nat">
            <option value="ind">Indonesia</option>
            <option value="sgn">Singapore</option>
            <option value="eng">English</option>
        </select> <br><br>
        <label for="language">Language Spoken :</label><br><br>
        <input type="radio" value="english"> English <br>
        <input type="radio" value="Indonesian"> Indonesian <br>
        <input type="radio" value="other"> Other <br><br>
        <label for="bio">Bio :</label><br><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Signup">


    </form>
</body>
</html>